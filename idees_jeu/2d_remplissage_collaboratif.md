# Remplissage collaboratif 2D

Le but est de remplir une forme en 2D de cubes.

Chaque joueur controle un cube qui peut se deplacer d'un pas, en haut, en bas, a gauche ou a droite, et celui ci laisse une trace derriere lui ce qui constitue le remplissage.

Il n'est pas possible de traverser un cube deja place, cela veut dire qu'un joueur peut se retrouver dans l'impossibilite de se deplacer car le cube est encercle.

Chaque joueur doit deplacer son cube d'un pas chaque tour, si le cube d'un joueur est bloque, alors il ne peut pas se deplacer durant le tour.

Le but du jeu est de remplir la forme en 2D de cubes en le moins de tours possible.